package edu.csu.hooheyhow.entity;

public class Die {

	private Face face;

	public Die() {
		face = Face.getRandom();
	}

	public Face getFace() {
		return face;
	}

	public void roll() {
		face = Face.getRandom();
	}

	public String toString() {
		return face.toString();
	}
}
