package edu.csu.hooheyhow.unittest;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import edu.csu.hooheyhow.entity.Die;
import edu.csu.hooheyhow.entity.Face;
import edu.csu.hooheyhow.entity.Punter;
import edu.csu.hooheyhow.utility.Round;

@ExtendWith(MockitoExtension.class)
class GameTest {

	@Mock
	Die mockDie1, mockDie2, mockDie3;

	/**
	 * objective: test bet lost amount is deducted properly from actual balance
	 * 
	 * pre-condition
	 * 
	 * 1. create valid player with tracked balance
	 * 
	 * 2. create a 3 die
	 * 
	 * 3. create a face which is not in the created die list
	 * 
	 * post-condition
	 * 
	 * 1. since the face is not in the die list, player should loose the bet and bet
	 * amount should be deducted from balance
	 */
	@Test
	void testBalanceAfterBetLost() {
		// arrange
		int initialBalance = 200;
		int betAmount = 20;
		// create player object with 200 initial balance
		Punter player = new Punter("PlayerName", initialBalance);
		// create a dies with random 3 die objects
		when(mockDie1.getFace()).thenReturn(Face.CRAB);
		when(mockDie2.getFace()).thenReturn(Face.FISH);
		when(mockDie3.getFace()).thenReturn(Face.ROOSTER);
		List<Die> dies = Arrays.asList(mockDie1, mockDie2, mockDie3);
		// create a face object which is not in the dies list, so that player looses the
		// bet
		Face nonMatchpick = Face.GOURD;
		// act
		int winningStatus = Round.play(player, dies, nonMatchpick, betAmount);
		// assert
		assertEquals(0, winningStatus);
		/**
		 * Since player have lost the bet initialBalance = 200 betAmount = 20 updated
		 * balance = initialBalance - betAmount = 180
		 */
		int updatedBalance = initialBalance - betAmount;
		assertEquals(updatedBalance, player.getBalance());
	}

	/**
	 * objective: test won amount is added properly in actual balance
	 * 
	 * pre-condition
	 * 
	 * 1. create valid player with tracked balance
	 * 
	 * 2. create a random dies
	 * 
	 * 3. create a face which is not in the created die list
	 * 
	 * post-condition
	 * 
	 * 1. since the face is in the die list, player should win the bet and bet
	 * amount should be added in balance
	 */
	@Test
	void testBalanceAfterBetWinWithOneMatch() {
		// arrange
		int initialBalance = 200;
		int betAmount = 20;
		int matches = 1;
		// create player object with 200 initial balance
		Punter player = new Punter("PlayerName", initialBalance);
		// create a dies with random 3 die objects
		when(mockDie1.getFace()).thenReturn(Face.CRAB);
		when(mockDie2.getFace()).thenReturn(Face.FISH);
		when(mockDie3.getFace()).thenReturn(Face.ROOSTER);
		List<Die> dies = Arrays.asList(mockDie1, mockDie2, mockDie3);
		// create a face object which is in the dies list, so that player win the
		// bet
		Face matchpick = Face.CRAB;
		// act
		int winningStatus = Round.play(player, dies, matchpick, betAmount);
		// assert: player won the match
		assertNotEquals(0, winningStatus);
		/**
		 * Since player have won the bet initialBalance = 200 betAmount = 20 updated
		 * balance = initialBalance + winningStatus = 220
		 */
		int updatedBalance = initialBalance + (betAmount * matches);
		assertEquals(updatedBalance, player.getBalance());
	}

	/**
	 * objective: test won amount is added properly in actual balance if two of the
	 * faces matches
	 * 
	 * pre-condition
	 * 
	 * 1. create valid player with tracked balance
	 * 
	 * 2. create a random dies
	 * 
	 * 3. create a face which is not in the created die list
	 * 
	 * post-condition
	 * 
	 * 1. since the face is in the die list, player should win the bet and bet
	 * amount should be added in balance
	 */
	@Test
	void testBalanceAfterBetWinWithTwoMatch() {
		// arrange
		int initialBalance = 200;
		int betAmount = 20;
		int matches = 2;
		// create player object with 200 initial balance
		Punter player = new Punter("PlayerName", initialBalance);
		// create a dies with random 3 die objects
		when(mockDie1.getFace()).thenReturn(Face.CRAB);
		when(mockDie2.getFace()).thenReturn(Face.CRAB);
		when(mockDie3.getFace()).thenReturn(Face.ROOSTER);
		List<Die> dies = Arrays.asList(mockDie1, mockDie2, mockDie3);
		// create a face object which is in the dies list, so that player win the
		// bet
		Face matchpick = Face.CRAB;
		// act
		int winningStatus = Round.play(player, dies, matchpick, betAmount);
		// assert: player won the match
		assertNotEquals(0, winningStatus);
		/**
		 * Since player have won the bet initialBalance = 200 betAmount = 20 updated
		 * balance = initialBalance + winningStatus = 220
		 */
		int updatedBalance = initialBalance + (betAmount * matches);
		assertEquals(updatedBalance, player.getBalance());
	}

	/**
	 * objective: test won amount is added properly in actual balance if three (all)
	 * of the faces matches
	 * 
	 * pre-condition
	 * 
	 * 1. create valid player with tracked balance
	 * 
	 * 2. create a random dies
	 * 
	 * 3. create a face which is not in the created die list
	 * 
	 * post-condition
	 * 
	 * 1. since the face is in the die list, player should win the bet and bet
	 * amount should be added in balance
	 */
	@Test
	void testBalanceAfterBetWinWithThreeMatch() {
		// arrange
		int initialBalance = 200;
		int betAmount = 20;
		int matches = 3;
		// create player object with 200 initial balance
		Punter player = new Punter("PlayerName", initialBalance);
		// create a dies with random 3 die objects
		when(mockDie1.getFace()).thenReturn(Face.CRAB);
		when(mockDie2.getFace()).thenReturn(Face.CRAB);
		when(mockDie3.getFace()).thenReturn(Face.CRAB);
		List<Die> dies = Arrays.asList(mockDie1, mockDie2, mockDie3);
		// create a face object which is in the dies list, so that player win the
		// bet
		Face matchpick = Face.CRAB;
		// act
		int winningStatus = Round.play(player, dies, matchpick, betAmount);
		// assert: player won the match
		assertNotEquals(0, winningStatus);
		/**
		 * Since player have won the bet initialBalance = 200 betAmount = 20 updated
		 * balance = initialBalance + winningStatus = 220
		 */
		int updatedBalance = initialBalance + (betAmount * matches);
		assertEquals(updatedBalance, player.getBalance());
	}

	/**
	 * objective: test won amount is added properly in actual balance
	 * 
	 * pre-condition
	 * 
	 * 1. create valid player with tracked balance and limit amount
	 * 
	 * post-condition
	 * 
	 * 1. player should able to bet up to betting limit
	 */

	@Test
	void testBalanceExceedLimit() {
		// arrange
		int initialBalance = 200;
		int limit = 100;
		Punter player = new Punter("PlayerName", initialBalance, limit);
		// act: check if player exceed limit if player bet 100
		boolean isBettingAllowed = player.balanceExceedsLimitBy(100);
		// assert: player should be allowed to bet till 100
		assertTrue(isBettingAllowed);
	}

	/**
	 * objective: every face is picked at least once while generating randomly.
	 * 
	 * pre-condition
	 * 
	 * 1. generate 100 random faces and store in an array
	 * 
	 * post-condition
	 * 
	 * 1. check the array contains every faces at least once.
	 */

	@Test
	void testRandomGenerationOfFaces() {
		// arrange: create an empty array list of type Face
		List<Face> faces = new ArrayList<Face>();
		// act: generate 100 random faces and store it in arraylist
		IntStream.rangeClosed(1, 100).forEach(i -> {
			faces.add(Face.getRandom());
		});
		// assert: stored arraylist at least contains one object of every faces
		Arrays.asList(Face.values()).stream().forEach(face -> {
			assertTrue("list doesnot contains " + face + " face.", faces.contains(face));
		});
	}

	/**
	 * objective: test roll method of die class. calling roll method of die object
	 * should update the face of that die object
	 * 
	 * pre-condition
	 * 
	 * 1. creating die object and keep calling roll method of that object multiple
	 * times
	 * 
	 * post-condition
	 * 
	 * 1. face of the die should change on every roll.
	 */

	@Test
	void testDieRoll() {
		// arrange
		boolean isChanging = false;
		Die die = new Die();
		Face initialFace = die.getFace();
		// act
		for (int i = 0; i < 10; i++) {
			die.roll();
			Face updatedFace = die.getFace();
			// check if initially assigned face has been changed after rolling the die or
			// not
			if (!updatedFace.equals(initialFace)) {
				isChanging = true;
				break;
			}
		}
		// assert: roll method of die class should keep updating the face on every roll
		assertTrue("rolling the die is not updating the initially assigned face '" + initialFace + "'", isChanging);

	}
}
